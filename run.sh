#!/usr/bin/env bash
xhost + && \
docker-compose down && \
docker-compose build && \
docker-compose up
