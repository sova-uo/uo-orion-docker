FROM ubuntu:20.04
ENV DEBIAN_FRONTEND=noninteractive

WORKDIR /orion/orion_launcher
RUN groupadd -g 1000 user && \
    useradd -rm -d /home/user -s /bin/bash -g user -G sudo -u 1000 user && \
    chown -R 1000:1000 /orion


RUN apt update && \
        apt install -y \
        libxcb* \
	libsm-dev \
	libxkbcommon-dev \
	libtiff-dev \
	libdrm-amdgpu1 \
	libfontconfig-dev \
	libx11-xcb-dev \
	libegl-dev \
	libdbus-1-3 \
	libharfbuzz-bin \
	libicu-dev \
	libasound2 \
	libpulse-dev \
	libsndio-dev \
	libxcursor-dev \
	libxinerama-dev \
	libxi-dev \
	libxrandr-dev \
	libxss-dev \
	libwayland-* \
	pulseaudio \
	telnet

# IT'S A MUST! Symlink of newer version breaks Orion
RUN echo "deb http://security.ubuntu.com/ubuntu bionic-security main" >> /etc/apt/sources.list && \
	apt-get update && \
	apt-get install libicu60 && \
	apt-get clean && \
	rm -rf /var/cache/apt/lists

RUN ln -sf /usr/lib/x86_64-linux-gnu/libsndio.so.7.0 /usr/lib/x86_64-linux-gnu/libsndio.so.6.1

USER user
COPY OrionLauncher/OrionLauncher64 .
ENTRYPOINT ["./OrionLauncher64"]

